#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.

This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.

Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py


See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
"""

# content of test_sample.py
def func(parameter):
    """
        oi
    """
    return parameter + 1


def test_answer():
    """
        oi
    """
    assert func(4) == 5
