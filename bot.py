#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""This module's docstring summary line.

This is a multi-line docstring. Paragraphs are separated with blank lines.
Lines conform to 79-column limit.

Module and packages names should be short, lower_case_with_underscores.
Notice that this in not PEP8-cheatsheet.py


See http://www.python.org/dev/peps/pep-0008/ for more PEP-8 details
"""

from selenium import webdriver
# from selenium.webdriver.common.keys import Keys


def main():
    """Write docstrings for ALL public classes, funcs and methods.

    Functions use snake_case.
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("useAutomationExtension", False)
    chrome_options.add_experimental_option("excludeSwitches",
                                           ["enable-automation"])
    driver = webdriver.Chrome(options=chrome_options, )

    driver.get(
    )
    form = driver.find_elements_by_xpath("//*[text() = 'fazer login']")
    for item in form:
        item.click()

    conta = driver.find_elements_by_xpath("//*[text() = 'Criar conta']")
    for item in conta:
        print("oi")

    driver.close()
    print("sample test case successfully completed")


if __name__ == "__main__":
    main()
